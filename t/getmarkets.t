#!/usr/bin/env perl

use diagnostics;
use warnings;
use strict;
use Data::Dumper;
use Test::More qw( no_plan ); # for the is() and isnt() function

# use lib '../';
use Polxtrader::Public::Market;

#------------------------------------------------------------------------------
# init trader class

my $trader = new Polxtrader::Public::Market->new;

#------------------------------------------------------------------------------
# test module subroutines

sub getmarkets_t {
    return $trader->getMarkets;
}

sub checksort_spread_t {
    my ( $btcrate, @sort ) = @_;
    @sort = reverse @sort;
    my $min = 0;
    foreach (@sort) {
        #print $_->{data}.':'."\n\t".'spreadpoints='.$_->{spreadpoints}."\n\t".'spread='.$_->{spread}.' ('.sprintf("%.2f", $_->{percent}).'%)'."\n\t".'spreadprice=$'.sprintf("%.4f", ($_->{spread}*$btcrate))."\n\t".'last='.$_->{last}."\n\t".'lowestAsk='.$_->{lowestAsk}."\n\t".'highestBid='.$_->{highestBid}."\n";
        my $val = $_->{spread};
        if ($val >= $min) {
            $min = $val;
        } else {
            return -1;
        }
    }
    return 0;
}

sub checksort_rank_t {
    my ( $btcrate, @sort ) = @_;
    @sort = reverse @sort;
    my $min = 0;
    foreach (@sort) {
        # print $_->{data}.':'."\n\t".'rank='.$_->{rank}."\n\t".'baseVolume='.$_->{baseVolume}."\n\t".'lowestAsk='.$_->{lowestAsk}."\n\t".'highestBid='.$_->{highestBid}."\n";
        my $val = $_->{rank};
        if ($val >= $min) {
            $min = $val;
        } else {
            return -1;
        }
    }
    return 0;
}

#------------------------------------------------------------------------------
# test module body

my @pairs = getmarkets_t;
my $btcrate = $trader->getBtcExchangeRate(@pairs);
my $sort_res_spread = checksort_spread_t($btcrate, $trader->sortBySpread(@pairs));
my $sort_res_rank   = checksort_rank_t($btcrate, $trader->sortByRank(@pairs));

ok($#pairs > 0, 'get markets list from poloniex'.($#pairs > 0 ? ': '.$#pairs : ''));
ok($btcrate > 0, 'BTC exchange rate'.($btcrate > 0 ? ': '.$btcrate : ''));
ok($sort_res_spread == 0, 'sorting spreads');
ok($sort_res_rank == 0, 'sorting ranks');

my %orders = $trader->getOrders;

ok($orders{asks}[0][0] > 0, 'get orders: asks='.$orders{asks}[0][0]);
ok($orders{bids}[0][0] > 0, 'get orders: bids='.$orders{bids}[0][0]);

my %savedata = ( myzec => '0.01990334', mybtc => '0.0005355210569', lastrate => '0.02506499', lastop => 'buy', cap => '0.001', timeout  => '30' );
ok ( $trader->setBalances('./t/json/balance.json', \%savedata ), 'set balances full data ');

%savedata = ( myzec => '0.023', mybtc => '0.1', lastrate => '0.02596499');
ok ( $trader->setBalances('./t/json/balance.json', \%savedata ), 'set balances particular data ');

my %balances = $trader->getBalances('./t/json/balance.json');

ok($balances{myzec} >= 0, 'get balances: myzec='.$balances{myzec});
ok($balances{mybtc} >= 0, 'get balances: mybtc='.$balances{mybtc});
ok($balances{lastop} =~ /(buy)|(sell)/, 'get balances: lastop='.$balances{lastop});
ok($balances{lastrate} >= 0, 'get balances: lastrate='.$balances{lastrate});
ok($balances{cap} >= 0, 'get balances: cap='.$balances{cap});
ok($balances{timeout} >= 0, 'get balances: timeout='.$balances{timeout});
