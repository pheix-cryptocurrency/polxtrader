# Polxtrader - simple scalper (scalping trader) for Poloniex

[![pipeline status](https://gitlab.com/pheix-cryptocurrency/polxtrader/badges/master/pipeline.svg)](https://gitlab.com/pheix-cryptocurrency/polxtrader/commits/master)

**Polxtrader** is released as a Perl package with unit tests.

#### Testing

```sh
prove -l ./lib -t ./t
```

#### License

Polxtrader is released under the [Artistic License 2.0](LICENSE).

#### Contact

Please get in touch [here](https://narkhov.pro/feedback.html).
