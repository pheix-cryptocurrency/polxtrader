#!/usr/bin/env perl

use diagnostics;
use strict;
use warnings;
use Data::Dumper;
use POSIX qw(strftime);
use List::Util qw(sum);

use lib '../lib/';
use Polxtrader::Public::Market;

my $trader    = new Polxtrader::Public::Market->new;
my $retcode   = $trader->setBalances('./balance.json', { cap => 0.000002, timeout => 30 } );
my $sellxrate = 1;
my $jitterlen = 4;  # 2 minutes jitter

$retcode = dumpLog(
    'Act',
    'Summa',
    'Rate',
    'Fromsell/Frombuy',
    'Fee',
    'Got',
    'Gotinbtc',
    'Goal',
);

while (1) {
    my %balances = $trader->getBalances('./balance.json');
    my %orders   = $trader->getOrders;
    if ($balances{lastop} eq 'buy') {
        my $bid = $orders{bids}[0][0] || 0;
        if ( $bid > 0 ) {
            my $btcfromsell = $balances{myzec} * $bid;
            my $fee         = $btcfromsell * 0.002;
            my $gotbtc      = $btcfromsell - $fee;

            if ( $gotbtc > ( $balances{myzec}*$balances{lastrate} + $balances{cap}*$sellxrate ) ) {
                # close previous sell orders
                # sell
                $retcode = playWithJitters($balances{myzec}, $bid, $balances{timeout}, 'sell');
                print '**ERR: playWithJitters fails, see ./jitters.log for details' . "\n" if !$retcode;
            } else {
                $retcode = dumpLog(
                    'SEL',
                    $balances{myzec},
                    $bid,
                    $btcfromsell,
                    sprintf("%.8f", $fee),
                    $gotbtc,
                    $gotbtc,
                    $balances{myzec}*$balances{lastrate} + $balances{cap}*$sellxrate,
                );
            }
            sleep($balances{timeout});
        }
    }
    elsif ($balances{lastop} eq 'sell') {
        my $ask = $orders{asks}[0][0] || 0;
        if ( $ask > 0 ) {
            my $zecfrombuy = $balances{mybtc}/$ask;
            my $fee        = $zecfrombuy * 0.001;
            my $gotzec     = $zecfrombuy - $fee;

                if ( $gotzec > ( $balances{mybtc} + $balances{mybtc}*0.002 + $balances{cap} )/$balances{lastrate} ) {
                # close previous buy orders
                # buy
                $retcode = playWithJitters($balances{mybtc}, $ask, $balances{timeout}, 'buy');
                print '**ERR: playWithJitters fails, see ./jitters.log for details' . "\n" if !$retcode;
            } else {
                $retcode = dumpLog(
                    'BUY',
                    $balances{mybtc},
                    $ask,
                    sprintf("%.8f", $zecfrombuy),
                    sprintf("%.8f", $fee),
                    $gotzec,
                    sprintf("%.8f", $gotzec*$ask),
                    ($balances{mybtc} + ($balances{mybtc}*0.002) + $balances{cap})/$balances{lastrate},
                );
            }
            sleep($balances{timeout});
        }
    } else {
        die 'unknown last operation:' . $balances{lastop};
    }
}

sub dumpTransaction {
    my $zec  = shift;
    my $btc  = shift;
    my $fee  = shift;
    my $rate = shift;
    my $oper = shift;
    my $fn = './transactions.log';
    my $date = strftime "%a %b %e %H:%M:%S", gmtime;
    open my $fh, ">>", $fn;
    print $fh join( q{|}, $date, $zec, $btc, $fee, $rate, $oper )."\n";
    close $fh;
    return 1;
}

sub dumpLog {
    my @data = @_;
    my $fn = './protocol.log';
    my $date = strftime "%a %b %e %H:%M:%S", gmtime;
    open my $fh, ">>", $fn;
    print $fh join( q{|}, $date, @data )."\n";
    close $fh;
    return 1;
}

sub dumpJitter {
    my @data = @_;
    my $fn = './jitter.log';
    my $date = strftime "%a %b %e %H:%M:%S", gmtime;
    open my $fh, ">>", $fn;
    print $fh join( q{|}, $date, @data )."\n";
    close $fh;
    return 1;
}

sub sell {
    my $summa       = shift;
    my $bid         = shift;
    my $oper        = 'sell';
    my $btcfromsell = $summa * $bid;
    my $fee         = $btcfromsell * 0.002;
    my $gotbtc      = $btcfromsell - $fee;
    dumpTransaction(
        q{-} . $summa,
        $gotbtc,
        sprintf("%.8f",$fee),
        $bid,
        $oper,
    );
    my %updbal = (
        myzec => 0,
        mybtc => $gotbtc,
        lastrate => $bid,
        lastop => $oper,
    );
    my $retcode = $trader->setBalances('./balance.json', \%updbal );
    print "sold $summa ZEC: bought $gotbtc BTC (exchange: $bid; fee: ".sprintf("%.8f",$fee).")\n";
    return 1;
}

sub buy {
    my $summa      = shift;
    my $ask        = shift;
    my $oper       = 'buy';
    my $zecfrombuy = $summa/$ask;
    my $fee        = $zecfrombuy * 0.001;
    my $gotzec     = $zecfrombuy - $fee;
    dumpTransaction(
        $gotzec,
        q{-} . $summa,
        sprintf("%.8f", $fee),
        $ask,
        $oper,
    );
    my %updbal = (
        myzec => $gotzec,
        mybtc => 0,
        lastrate => $ask,
        lastop => $oper,
    );
    $retcode  = $trader->setBalances('./balance.json', \%updbal );
    print "bought $gotzec ZEC: sold $summa BTC (exchange: $ask; fee: ".sprintf("%.8f",$fee).")\n";
    return 1;
}

sub playWithJitters {
    my $summa       = shift;
    my $initialrate = shift;
    my $timeout     = shift;
    my $oper        = shift;
    my $checkrate   = $initialrate;
    my $retcode     = 0;
    my @jitters;
    if ($initialrate > 0 && $oper =~ /(buy)|(sell)/) {
        while(1) {
            my %orders   = $trader->getOrders;
            if ( $oper eq 'sell' ) {
                my $bid = $orders{bids}[0][0] || 0;
                if ( $#jitters == ( $jitterlen-1 ) ) {
                    my $average = sum(@jitters)/@jitters;
                    my $checkdt = $checkrate - $average;
                    if ( $average >= $checkrate || ( $checkdt > 0 && $checkdt < 0.0001 ) ) {
                        @jitters   = ();
                        $checkrate = $average;
                        # log: new jitters collection started with new $checkrate
                        dumpJitter(
                            'SEL',
                            $initialrate,
                            'new jitters dataset',
                            sprintf("%.8f", $checkrate),
                        );
                    } else {
                        if ( $bid >= $initialrate) {
                            sell( $summa, $bid );
                            # log: determine trend reverse: $average < $checkrate, sold by $bid
                            dumpJitter(
                               'SEL',
                                $initialrate,
                                'trend reverse found',
                                sprintf("%.8f", $average) . q{;}. $bid,
                            );
                            $retcode = 1;
                            last;
                        }
                        else {
                            if ($bid > 0) {
                                sell( $summa, $bid );
                                my $i = 0;
                                # log: lost deal (fast jitter) $bid, $average, $checkrate, @jitters
                                dumpJitter(
                                   'SEL',
                                    $initialrate,
                                    'forced sell at lost deal',
                                    join(
                                        q{;},
                                        $bid,
                                        sprintf("%.8f", $average),
                                        sprintf("%.8f", $checkrate),
                                        map {
                                            $jitters[$i++] . (
                                                $jitters[$i-1] > ( $i == 1 ? $initialrate : $jitters[$i-2] ) ? '↗' :
                                                    ( $jitters[$i-1] == ($i == 1 ? $initialrate : $jitters[$i-2]) ? '→' : '↘' )
                                            )
                                        } @jitters
                                    ),
                                );
                                last;
                            }
                        }
                    }
                }
                else {
                    my $i = 0;
                    push( @jitters, $bid );
                    # log: collect jitters no.($#jitters+1)
                    dumpJitter(
                        'SEL',
                        $initialrate,
                        'collect jitter no.' . ($#jitters+1),
                        join(
                            q{;},
                            map {
                                $jitters[$i++] . (
                                    $jitters[$i-1] > ( $i == 1 ? $initialrate : $jitters[$i-2] ) ? '↗' :
                                        ( $jitters[$i-1] == ($i == 1 ? $initialrate : $jitters[$i-2]) ? '→' : '↘' )
                                )
                            } @jitters
                        ),
                    );
                }
            }
            elsif ( $oper eq 'buy' ) {
                my $ask = $orders{asks}[0][0] || 0;
                if ( $#jitters == ( $jitterlen-1 ) ) {
                    my $average = sum(@jitters)/@jitters;
                    my $checkdt = $average - $checkrate;
                    if ( $average <= $checkrate || ( $checkdt > 0 && $checkdt < 0.0001 ) ) {
                        @jitters   = ();
                        $checkrate = $average;
                        # log: new jitters collection started with new $checkrate ($initialrate)
                        dumpJitter(
                            'BUY',
                            $initialrate,
                            'new jitters dataset',
                            sprintf("%.8f", $checkrate),
                        );
                    } else {
                        if ( $ask >= $initialrate) {
                            buy( $summa, $ask );
                            # log: determine trend reverse: $average > $checkrate, bought by $ask ($initialrate)
                            dumpJitter(
                                'BUY',
                                $initialrate,
                                'trend reverse found',
                                sprintf("%.8f", $average) . q{;} . $ask,
                            );
                            $retcode = 1;
                            last;
                        }
                        else {
                            if ($ask > 0) {
                                buy( $summa, $ask );
                                my $i = 0;
                                # log: lost deal (fast jitter) $ask, $average, $checkrate, $initialrate, @jitters
                                dumpJitter(
                                    'BUY',
                                    $initialrate,
                                    'forced buy at lost deal',
                                    join(
                                        q{;},
                                        $ask,
                                        sprintf("%.8f", $average),
                                        sprintf("%.8f", $checkrate),
                                        map {
                                            $jitters[$i++] . (
                                                $jitters[$i-1] > ( $i == 1 ? $initialrate : $jitters[$i-2] ) ? '↗' :
                                                    ( $jitters[$i-1] == ($i == 1 ? $initialrate : $jitters[$i-2]) ? '→' : '↘' )
                                            )
                                        } @jitters
                                    ),
                                );
                                last;
                            }
                        }
                    }
                }
                else {
                    my $i = 0;
                    push(@jitters, $ask);
                    # log: collect jitters no.($#jitters+1)
                    dumpJitter(
                        'BUY',
                        $initialrate,
                        'collect jitter no.' . ($#jitters+1),
                        #join(q{;}, @jitters),
                        join(
                            q{;},
                            map {
                                $jitters[$i++] . (
                                    $jitters[$i-1] > ( $i == 1 ? $initialrate : $jitters[$i-2] ) ? '↗' :
                                        ( $jitters[$i-1] == ($i == 1 ? $initialrate : $jitters[$i-2]) ? '→' : '↘' )
                                )
                            } @jitters
                        ),
                    );
                }

            }
            else {
                print 'unknown operation ' . $oper . "\n";
                last;
            }
            sleep($timeout);
        }
    }
    return $retcode;
}
