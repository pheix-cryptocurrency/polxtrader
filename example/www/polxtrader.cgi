#!/usr/bin/env perl

use strict;
use warnings;
use diagnostics;
use CGI;

my @tab;
my $path  = '/home/kostas/git/pheix-cryptocurrency/polxtrader/example';
my $trans = $path . '/transactions.log';
my $balns = $path . '/balance.json';
my $protl = $path . '/protocol.log';
my $jitrs = $path . '/jitter.log';

my $co = CGI->new;
print $co->header(-charset=>'utf-8');
print $co->start_html(
    -title=> 'Polxtrader statistics',
    -meta => {
        'viewport'=> 'width=device-width, initial-scale=1, maximum-scale=1',
        'apple-mobile-web-app-capable'=>'yes',
    },
    -style => {
        -verbatim => qq~
            hr { border: none; color:transparent; margin: 20px 0; border-bottom: 2px dotted pink !important; }
            body { font-family: 'Roboto', sans-serif; }
            table { border-collapse: collapse; width:100%; }
            table,th,td,tr { border: 1px solid #ccc; padding:5px; white-space:nowrap}
            a:visited, a { color: #660000; text-decoration:none; border-bottom: 1px dotted #660000;}
            a:hover, a:active, a.focused { color: red; text-decoration:none; border-bottom: 1px dotted red;}
            .session { background-color: #c1f0c1; }
            .linkblock {display: inline-block; text-align:center; padding:10px 0; line-height: 25px; width:80px; background-color: #f9f9f9; }
            .linkblock:hover { background-color: #f1f1f1; }
            .taboverflow { width:100%; overflow:auto; }
            .accept  { color: #4298f4; }
            .accept2 { color: #47aa33; }
        ~,
        -src => 'https://fonts.googleapis.com/css?family=Roboto',
    }
);

print qq~
    <a id="top"></a>
    <hr>
    <div class="linkblock"><a href="javascript:top.history.back();">Back &#8617;</a></div>
    <div class="linkblock"><a href="$ENV{SCRIPT_NAME}?filter=50">50</a></div>
    <div class="linkblock"><a href="$ENV{SCRIPT_NAME}?filter=200">200</a></div>
    <div class="linkblock"><a href="$ENV{SCRIPT_NAME}?filter=0">All</a></div>
    <hr>
~;

print doVisualize('Transactions', $trans);
print doVisualize('Attempts protocol', $protl, $co->param('filter'));
print doVisualize('Jitters protocol', $jitrs, 2 * $co->param('filter'));
print '<hr><div class="linkblock"><a href="#top">Top &uarr;</a></div>' . $co->end_html;

sub doVisualize {
    my $hdr   = shift || 'Untitled table';
    my $file  = shift || 0;
    my $depth = shift || -1;
    my $ret;
    my @tab;
    if ( $file && (-e $file)) {
        open( my $fh, '<', $file );
        my @data   = reverse <$fh>;
        my $legnd  = (map { $_ } grep { $_ =~ /Summa/ } @data)[0];
        $legnd =~ s/([\s\da-z\:]+)\|/Date\|/i;
        close($fh);
        foreach my $row ( $legnd, @data[0..($depth > 0 ? $depth : $#data)] ) {
            chomp($row);
            my @_cols  = split(/\|/, $row);
            my $clss   = 'session' if $row =~ /Summa/;
            my $myrt   = getMyRate(@_cols) if $file !~ /jitter/;
            $_cols[3] .=
                q{/} .
                '<span class="accept' . ( $_cols[1] eq 'SEL' ? q{2} : q{} ) . '">' .
                sprintf("%.8f", $myrt) . '</span>' if $myrt > 0;
            push(
                @tab,
                '<tr class="'. $clss .'"><td>' . join('</td><td>', @_cols) . '</td></tr>'
            ) if @_cols;
        }
        $ret = '<h3>' . $hdr . '</h3><div class="taboverflow"><table>' . join( "\n", @tab ) . '</table></div>';
    }
    return $ret;
}

sub getMyRate {
    my @_cols = @_;
    my $rate;
    if ( $_cols[1] eq 'BUY' ) {
        $rate = ($_cols[2] - $_cols[2]*0.001)/$_cols[-1]
    }
    elsif ( $_cols[1] eq 'SEL' ) {
        $rate = $_cols[-1]/($_cols[2] - $_cols[2]*0.002)
    } else {
        $rate = -1;
    }
    return $rate;
}
