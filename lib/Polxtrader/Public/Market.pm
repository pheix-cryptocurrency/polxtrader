package Polxtrader::Public::Market;

use strict;
use warnings;

BEGIN {
    use lib '../../';
}

##############################################################################
#
# File   :  Market.pm
#
##############################################################################

use POSIX;
use Encode;
use utf8;
use open qw(:std :utf8);
use Data::Dumper;
use LWP::UserAgent;
use JSON::XS;

#------------------------------------------------------------------------------

my %commands = (
    markets => 'returnTicker',     # Returns the ticker for all markets
    vol24   => 'return24hVolume',  # Returns the 24-hour volume for all markets + totals for primary currencies
    orderbook => 'returnOrderBook',  # Returns the order book for a given market
);

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# new()
#------------------------------------------------------------------------------
#
# Constructor of Polxtrader::Public::Market
#
#------------------------------------------------------------------------------

sub new {
    my $class = shift;
    my $self = {
        name       => 'Polxtrader::Public::Market',
        revision   => 'init_1.0',
        urlmarkets => 'https://poloniex.com/public?command=',
        urlorders  => 'https://poloniex.com/public?command=<com>&currencyPair=<pair>&depth=<depth>'
    };
    bless $self, ref($class) || $class;
    return $self;
}

#------------------------------------------------------------------------------

sub getMarkets {
    my $self   = shift;
    my $rc     = 0;
    my $ua     = LWP::UserAgent->new;
    my $time   = strftime "%H:%M:%S", gmtime;
    $ua->timeout(10);
    $ua->env_proxy;
    my $response = $ua->get( $self->{urlmarkets} . $commands{markets} );
    if ( $response->is_success ) {
        if ( my $dec = JSON::XS::decode_json( $response->decoded_content ) ) {
            my @pairs;
            foreach my $pair_data (keys %{$dec}) {
                 my %_prs = (data => $pair_data, data_hash => $dec->{$pair_data} );
                 push(@pairs, \%_prs);
            }
            return @pairs;
        }
        else { die '['.$time.'] **ERR: could not decode json data'; }
    }
    else {
        die '['.$time.']**ERR: poloniex public api response ' . $response->status_line;
    }
    return;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# sortBySpread()
#------------------------------------------------------------------------------
#
# Sort markets list by spread
#
#------------------------------------------------------------------------------

sub sortBySpread {
    my ($self, @markets) = @_;
    my @_spreadsort;
    foreach my $pairs (@markets) {
        if ($pairs->{data} !~ /^BTC\_/ || $pairs->{data} =~ /^USDT\_/ ) {
            next;
        }
        my $_ask   = $pairs->{data_hash}->{lowestAsk};
        my $_bid   = $pairs->{data_hash}->{highestBid};
        my $_delta  = ( $_ask - $_bid );
        my $_percnt = $_delta * 100 / $pairs->{data_hash}->{last};
        my %_shash = ( 
            data => $pairs->{data},
            spreadpoints => ($self->floatToFrac($_ask) - $self->floatToFrac($_bid)),
            spread => $_delta,
            percent => $_percnt,
            lowestAsk => $_ask,
            highestBid => $_bid,
            last => $pairs->{data_hash}->{last},
        );
        push(@_spreadsort, \%_shash);
    }
    my @sorted = sort { $b->{spread} <=> $a->{spread} } @_spreadsort;
    return @sorted;
}

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# sortByRank()
#------------------------------------------------------------------------------
#
# Sort markets list by rank: R = ((ask-bib)/bid)*volume
#
#------------------------------------------------------------------------------

sub sortByRank {
    my ($self, @markets) = @_;
    my @_ranksort;
    foreach my $pairs (@markets) {
        if ($pairs->{data} !~ /^BTC\_/ || $pairs->{data} =~ /^USDT\_/ ) {
            next;
        }
        my $_ask   = $pairs->{data_hash}->{lowestAsk};
        my $_bid   = $pairs->{data_hash}->{highestBid};
        my $_vol   = $pairs->{data_hash}->{baseVolume};
        my $_rank  = (( $_ask - $_bid )/$_bid )*$_vol;
        my %_rhash = ( 
            data => $pairs->{data},
            rank => $_rank,
            lowestAsk => $_ask,
            highestBid => $_bid,
            last => $pairs->{data_hash}->{last},
            baseVolume => $_vol,
        );
        push(@_ranksort, \%_rhash);
    }
    my @sorted = sort { $b->{rank} <=> $a->{rank} } @_ranksort;
    return @sorted;
}

#------------------------------------------------------------------------------

sub getBtcExchangeRate {
    my ($self, @markets) = @_;
    my $btcrate = 0;
    foreach my $pairs (@markets) {
        if ($pairs->{data} =~ /^USDT\_BTC$/) {
            $btcrate = $pairs->{data_hash}->{last};
            last;
        }
    }
    return $btcrate;
}

#------------------------------------------------------------------------------

sub floatToFrac {
    my ($self, $value) = @_;
    my @_tmp = split /\./, $value;
    return floor($_tmp[1]);
}

#------------------------------------------------------------------------------

sub getOrders {
    my $self  = shift;
    my $pair  = shift || 'BTC_ZEC';
    my $depth = shift || 1;
    my $rc   = 0;
    my $ua   = LWP::UserAgent->new;
    my $time = strftime "%H:%M:%S", gmtime;
    $self->{urlorders} =~ s/\<com\>/$commands{orderbook}/;
    $self->{urlorders} =~ s/\<pair\>/$pair/;
    $self->{urlorders} =~ s/\<depth\>/$depth/;
    $ua->timeout(10);
    $ua->env_proxy;
    my $response = $ua->get( $self->{urlorders} );
    if ( $response->is_success ) {
        if ( my $dec = JSON::XS::decode_json( $response->decoded_content ) ) {
            my %_orders;
            foreach my $key (keys %{$dec}) {
                 $_orders{$key} = $dec->{$key} if $key =~ /(asks)|(bids)/;
            }
            # print Dumper(%_orders);
            return %_orders;
        }
        else {
            # die '**ERR: could not decode json data';
            print '['.$time.'] **ERR: could not decode json data' . "\n";
        }
    }
    else {
        # die '**ERR: poloniex public api response ' . $response->status_line;
        print '['.$time.'] **ERR: poloniex public api response ' . $response->status_line . "\n";
    }
    return;
}

#------------------------------------------------------------------------------

sub getBalances {
    my $self = shift;
    my $file = shift || 0;
    my $time = strftime "%H:%M:%S", gmtime;
    my %balance;
    if (-e $file) {
        open(
            my $fh,
            '<',
            $file
        ) or die 'could not open file ' .$file . ': ' . $!;
        if ( my $dec = JSON::XS::decode_json( join('',<$fh>) ) ) {
            return %$dec;
        }
        else {
            die '['.$time.'] **ERR: could not decode json data';
        }
        close($fh);
    }
    else {
        die '['.$time.'] **ERR: could not open file: ' . $file;
    }
}

#------------------------------------------------------------------------------

sub setBalances {
    my $self = shift;
    my $file = shift || 0;
    my $hashref = shift; # || { myzec => 0, mybtc => 0, lastop => 'sell', lastrate => 0, cap => 0, timeout => 60 };
    if ( $file ) {
        # my %curr = $self->getBalances($file) if (-e $file);
        my %curr;
        if (-e $file) {
            %curr = $self->getBalances($file) if (-e $file);
        }
        else {
            %curr = ( myzec => '0.02', mybtc => 0, lastop => 'buy', lastrate => '0.02501001', cap => '0.001', timeout => 20 );
        }
        open(
            my $fh,
            '>',
            $file
        ) or die 'could not open file ' .$file . ': ' . $!;
        if ( exists $hashref->{myzec} ) { $curr{myzec} = $hashref->{myzec}; }
        if ( exists $hashref->{mybtc} ) { $curr{mybtc} = $hashref->{mybtc}; }
        if ( exists $hashref->{lastop} ) { $curr{lastop} = $hashref->{lastop}; }
        if ( exists $hashref->{lastrate} ) { $curr{lastrate} = $hashref->{lastrate}; }
        if ( exists $hashref->{cap} ) { $curr{cap} = $hashref->{cap}; }
        if ( exists $hashref->{timeout} ) { $curr{timeout} = $hashref->{timeout}; }
        if ( my $enc = JSON::XS::encode_json( \%curr ) ) {
            print $fh $enc;
            close($fh);
            return 1;
        }
        else {
            # die '**ERR: could not encode json data';
            print '**ERR: could not encode json data' . "\n";
        }
    }
    else {
        die '**ERR: please give filename to store balance';
    }
    return;
}

#------------------------------------------------------------------------------

END { }

1;
